#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "deque.h"

DECLARE_DEQUE(int)

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

int main(int argc, char **argv) {
        int data[] = {1, 2, 3, 4, 5, 6, 7};
        DEQUE(int) *dq = alloc_deque_int(ARRAY_SIZE(data));
        if (!dq) {
                perror("malloc");
                return -1;
        }

        int i;
        for (i = 0; i < ARRAY_SIZE(data); i++) {
                if (!push_int(dq, data + i)) {
                        puts("failed to push");
                        return -1;
                }
        }

        for (i = 0; i < ARRAY_SIZE(data); i++) {
                int *t = pop_int(dq);
                if (!t) {
                        puts("failed to pop");
                        return -1;
                }
                printf("%d\n", *t);
        }
        free_deque_int(dq);
        return 0;
}
