#ifndef __PO_VAAPI_DEQUE_H__
#define __PO_VAAPI_DEQUE_H__

#define DEQUE(__of_type) deque_ ## __of_type ## t

#define DECLARE_DEQUE(__of_type) \
 \
typedef struct deque_ ## __of_type { \
        __of_type **data; \
        size_t size; \
        size_t num_items; \
        size_t start_offset; \
} deque_ ## __of_type ## t; \
 \
static inline int push_ ## __of_type (DEQUE(__of_type) *dq, __of_type *data) { \
        if ((NULL == dq) || (dq->num_items >= dq->size)) { \
                return 0; \
        } \
 \
        dq->data[(dq->start_offset + dq->num_items) % dq->size] = data; \
        dq->num_items++; \
        return 1; \
} \
 \
static inline __of_type *pop_ ## __of_type (DEQUE(__of_type) *dq) { \
        __of_type *ret = NULL; \
        if ((NULL == dq) || (0 == dq->num_items)) { \
                return NULL; \
        } \
 \
        ret = dq->data[(dq->start_offset) % dq->size]; \
        dq->start_offset++; \
        dq->num_items--; \
        return ret; \
} \
 \
static inline DEQUE(__of_type)* alloc_deque_ ## __of_type (size_t size) { \
        DEQUE(__of_type) *dq = (DEQUE(__of_type)*)malloc(sizeof(DEQUE(__of_type))); \
        if (!dq) { \
                goto fail; \
        } \
        memset(dq, 0, sizeof(DEQUE(__of_type))); \
 \
        dq->data = (__of_type**)malloc(sizeof(__of_type*) * size); \
        if (!dq->data) { \
                goto fail; \
        } \
 \
        dq->size = size; \
         \
        return dq; \
 \
fail: \
        if (dq) { \
                free(dq); \
        } \
        return NULL; \
} \
 \
static inline void free_deque_ ## __of_type (DEQUE(__of_type) *dq) { \
        if (!dq) { \
                return; \
        } \
        if (dq->data) { \
                free(dq->data); \
        } \
        free(dq); \
}

#endif //__PO_VAAPI_DEQUE_H__
