APPNAME=list
CC?=gcc
CFLAGS=-pg -O2 -g2 -Wall
LDFLAGS=

CFILES = list.c

OBJFILES=$(patsubst %.c,%.o,$(CFILES))

all: $(APPNAME)

$(APPNAME): $(OBJFILES)
	$(CC) $(LDFLAGS) $(CFLAGS) -o $@ $(OBJFILES)

$(OBJFILES): %.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm $(APPNAME) *.o || true

run:
	make clean
	make all
	./$(APPNAME)
